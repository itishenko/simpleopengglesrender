//
//  GLViewController.h
//  HelloOpenGL
//
//  Created by itishenko on 29/04/15.
//
//

#import <UIKit/UIKit.h>
#import "OpenGLView.h"

@interface GLViewController : UIViewController{
    OpenGLView* _glView;
}


@property (nonatomic, retain) IBOutlet OpenGLView *glView;
@end
