#include "cpprender.h"
#include <stdlib.h>


SimpleCppRender::SimpleCppRender(){
    Init();
}

///
// Create a shader object, load the shader source, and
// compile the shader.
//
GLuint SimpleCppRender::LoadShader(const char *shaderSrc, GLenum type)
{
    GLuint shader;
    GLint compiled;
    
    // Create the shader object
    shader = glCreateShader(type);
    
    if(shader == 0)
        return 0;
    
    // Load the shader source
    glShaderSource(shader, 1, &shaderSrc, NULL);
    
    // Compile the shader
    glCompileShader(shader);
    
    // Check the compile status
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    
    if(!compiled)
    {
        GLint infoLen = 0;
        
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
        
        if(infoLen > 1)
        {
            char* infoLog = (char*)malloc(sizeof(char) * infoLen);
            
            glGetShaderInfoLog(shader, infoLen, NULL, infoLog);
            printf("Error compiling shader:\n%s\n", infoLog);
            free(infoLog);
        }
        
        glDeleteShader(shader);
        return 0;
    }
    
    return shader;
    
}

///
// Initialize the shader and program object
//
int SimpleCppRender::Init()
{
    const char* vShaderStr =
    "attribute vec4 vPosition;   \n"
    "void main()                 \n"
    "{                           \n"
    "  gl_Position = vPosition;  \n"
    "}                           \n";
    
    const char*  fShaderStr =
    "precision mediump float;                   \n"
    "void main()                                \n"
    "{                                          \n"
    "  gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0); \n"
    "}                                          \n";

    GLuint framebuffer;
    GLuint colorRenderBuffer;
    GLuint vertexShader;
    GLuint fragmentShader;
    GLint linked;
    
    // setup render buffer
    glGenRenderbuffers(1, &colorRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    
    //setup frame buffer
    glGenFramebuffers(1, &framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
    
    // Load the vertex/fragment shaders
    vertexShader = LoadShader(vShaderStr, GL_VERTEX_SHADER);
    fragmentShader = LoadShader(fShaderStr, GL_FRAGMENT_SHADER);
    
    // Create the program object
    programObject = glCreateProgram();
    
    if(programObject == 0)
        return 0;
    
    glAttachShader(programObject, vertexShader);
    glAttachShader(programObject, fragmentShader);
    
    // Bind vPosition to attribute 0
    glBindAttribLocation(programObject, 0, "vPosition");
    
    // Link the program
    glLinkProgram(programObject);
    
    // Check the link status
    glGetProgramiv(programObject, GL_LINK_STATUS, &linked);
    
    if(!linked)
    {
        GLint infoLen = 0;
        
        glGetProgramiv(programObject, GL_INFO_LOG_LENGTH, &infoLen);
        
        if(infoLen > 1)
        {
            char* infoLog = (char*)malloc(sizeof(char) * infoLen);
            
            glGetProgramInfoLog(programObject, infoLen, NULL, infoLog);
            printf("Error linking program:\n%s\n", infoLog);
            
            free(infoLog);
        }
        
        glDeleteProgram(programObject);
        return false;
    }
    

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    return true;
}

///
// Draw a triangle using the shader pair created in Init()
//
void SimpleCppRender::Draw()
{
    GLfloat vVertices[] = {0.0f,  0.5f, 0.0f,
        -0.5f, -0.5f, 0.0f,
        0.5f, -0.5f,  0.0f};
    
    // Clear the color buffer
    glClear(GL_COLOR_BUFFER_BIT);
    
    // Use the program object
    glUseProgram(programObject);
    
    // Load the vertex data
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, vVertices);
    glEnableVertexAttribArray(0);
    
    glDrawArrays(GL_TRIANGLES, 0, 3);
}