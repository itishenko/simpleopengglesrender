#ifndef __HelloOpenGL__cpprender__
#define __HelloOpenGL__cpprender__

#include <stdio.h>
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>


class SimpleCppRender {
private:
    GLuint programObject;
    GLuint LoadShader(const char *shaderSrc, GLenum type);
    int Init();
public:
    SimpleCppRender();
    ~SimpleCppRender(){}
    void Draw();
};

#endif /* defined(__HelloOpenGL__cpprender__) */
